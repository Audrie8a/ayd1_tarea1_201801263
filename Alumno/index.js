var express = require('express');
const morgan=require('morgan');
var bodyParser = require('body-parser');
var app = express();
const cors = require('cors');
var corsOptions = { origin: true, optionsSuccessStatus: 200 };

app.use(morgan('dev'))
app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))
var port = 4000;
app.listen(port);
console.log('Listening on port', port);


//Rutas
app.get('/Alumno', function(req,res){
res.send("Audrie Annelisse del Cid Ochoa - 201801263");
});
