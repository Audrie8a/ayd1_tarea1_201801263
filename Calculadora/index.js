var express = require('express');
const morgan=require('morgan');
var bodyParser = require('body-parser');
var app = express();
const cors = require('cors');
var corsOptions = { origin: true, optionsSuccessStatus: 200 };

app.use(morgan('dev'))
app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))
var port = 3000;
app.listen(port);
console.log('Listening on port', port);


//Rutas
app.get('/', function(req,res){
res.send("Bienvenido!")
});

app.post('/Suma',async(req, res)=>{
  const {a, b}= req.body;
  const ResultSuma = Number.isInteger(a) && Number.isInteger(b) ? a+b: "Error en la Suma. Valores no Enteros." 
  
  res.json({Resultado: ResultSuma});
})

app.post('/Resta',async(req, res)=>{
  const {a, b}= req.body;
  const ResultRest = Number.isInteger(a) && Number.isInteger(b) ? a-b: "Error en la Resta. Valores no Enteros." 
  
  res.json({Resultado: ResultRest});
})

app.post('/Div',async(req, res)=>{
  const {a, b}= req.body;
  const ResultDiv = Number.isInteger(a) && Number.isInteger(b) ? a/b: "Error en la División. Valores no Enteros." 
  
  res.json({Resultado: ResultDiv});
})

